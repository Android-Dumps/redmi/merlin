## merlin-user 11 RP1A.200720.011 V12.5.6.0.RJOINXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: merlin
- Brand: Redmi
- Flavor: merlin-user
merlin-user
merlinnfc-user
- Release Version: 11
- Kernel Version: 4.14.186
- Id: RP1A.200720.011
- Incremental: V12.5.6.0.RJOINXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/merlin/merlin:11/RP1A.200720.011/V12.5.6.0.RJOINXM:user/release-keys
- OTA version: 
- Branch: merlin-user-11-RP1A.200720.011-V12.5.6.0.RJOINXM-release-keys
- Repo: redmi/merlin
